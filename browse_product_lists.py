from selenium import webdriver
import os
import time

starting_url = "http://www.amazon.com/s/ref=sr_nr_n_1?rh=n%3A6459737011%2Ck%3Atv&keywords=tv&ie=UTF8&qid=1394213822&rnid=2941120011"
br = webdriver.Firefox()
br.get(starting_url)
time.sleep(2)

# Download the html and click "next page" a few times.
num_pages = 100
for i in range(0,num_pages):
  time.sleep(2)
  out = open('results/list_%d.html' % i, 'w')
  out.write(br.page_source.encode('utf-8'))
  out.close()
  if i == num_pages - 1:
    break
  # Find the "next page".
  next_page = br.find_element_by_xpath('//span[@id="pagnNextString"]')
  next_page.click()

br.close()
