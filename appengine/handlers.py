from model import *

from google.appengine.ext import ndb
from google.appengine.api import mail
from google.appengine.api import runtime
from google.appengine.api import taskqueue
import time
import re
import webapp2
import logging
from datetime import datetime, timedelta
import urllib
import urllib2
import httplib
import os
import jinja2
import json
import sys
from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.5f')

JINJA_ENV = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

num_results_to_return = 100

class Search(webapp2.RequestHandler):
  def get(self):
    q = self.request.get('search', None)
    if not q:
      template = JINJA_ENV.get_template('main.html')
      params = {}
      self.response.write(template.render(params))
      return
    q = q.lower()
    terms = q.split(' ')
    ndb_query = BriefProduct.query()
    for term in terms:
      ndb_query = ndb_query.filter(BriefProduct.title_fields == term)
    # self.response.write(str(ndb_query.count()) + "<br>")
    ndb_query = ndb_query.order(BriefProduct.current_price_num)
    results = ndb_query.fetch(num_results_to_return)
    # for result in results:
    #   self.response.write(result.title + " " + str(result.current_price) + "<br>")

    #self.response.write(json.dumps([r.to_dict() for r in results]))
    html = ""
    for result in results:
      html += '<div class="title"><img src="%s"/><a href="%s">%s</a></div>' % (
          result.img if result.img else "",
          result.url if result.url else "",
          result.title if result.title else "No title")
      html += '<div class="price">%s, size: %d inch, resolution: %s, smart: %s, slim: %s, type: %s, Hz: %s, Brand: %s, Rating: %s</div>' % (
          str(result.current_price),
          (result.inch_num) if result.inch_num else -1,
          result.resolution,
          str(result.smart),
          str(result.slim),
          str(result.tv_type),
          str(result.hz),
          str(result.brand),
          str(result.rating))
    self.response.write(html)

class PrecomputeSimilarProduct(webapp2.RequestHandler):
  def get(self):
     # emit tasks
    if self.request.get('emit'):
      logging.info("emit")
      q = BriefProduct.query()
      count = 0
      self.response.write("Submitting tasks...")
      for product0 in q.iter():
        count += 1
        taskqueue.add(url='/precompute_similar_product',
            method='POST',
            params={'id': product0.key.id()})
      self.response.write("%d tasks submitted" % count)

  def post(self):
    pid = self.request.get('id')
    if not pid:
      return
    k = ndb.Key('BriefProduct', pid)
    product0 = k.get()
    q = BriefProduct.query()
    sp = SimilarProduct(id=pid)
    neighbors = []
    for product in q.iter():
      if self.is_similar(product0, product):
        neighbors.append(product.key.id())
    sp.neighbors = str(neighbors)
    ndb.put(sp)

  def is_similar(self, p1, p2):
    dis = 0
    if p1.brand != p2.brand:
      dis += 1
    if p1.inch != p2.inch:
      dis += 2
    if p1.resolution != p2.resolution:
      dis += 2
    if p1.slim != p2.slim:
      dis += 2
    if p1.hz != p2.hz:
      dis += 1
    return dis <= 3

class ViewProduct(webapp2.RequestHandler):
  def get(self):
    self.response.write("View product")
