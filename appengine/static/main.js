/**/
$("#searchform").submit(function(event) {
  var query = $("searchinput").val();
  do_search(query);
}
    );

function do_search(query) {
  url = "/search?query=" + query;
  $.getJSON(url, function(data) {
    $("#results").html("");
    for (var i = 0; i < data.length; i++) {
      $("#results").append("<div class=\"product\"><a href=\"" + data.link +
        "\">" + data.title + "</a><div class=\"price\">" +
        data.current_price + "</div></div>");
    }
  });
}
