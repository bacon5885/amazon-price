from model import *
from handlers import * 

from google.appengine.ext import ndb
from google.appengine.api import mail
from google.appengine.api import runtime
from google.appengine.api import taskqueue
import time
import re
import webapp2
import logging
from datetime import datetime, timedelta
import urllib
import urllib2
import httplib
import os
import jinja2
import json
import sys
from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.5f')

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])

class UploadBriefProduct(webapp2.RequestHandler):
  def get(self):
    path = os.path.join(os.path.split(__file__)[0], 'brief_product_info.txt')
    f = open(path, 'r')
    count = 0
    product = None
    list_entities = []
    for line in f:
      if len(list_entities) > 20:
        ndb.put_multi(list_entities)
        #logging.info("put 20")
        list_entities = []
      splits = line.strip(' \n').split(': ')
      value = splits[1] if len(splits) > 1 else None
      if line.startswith('link'):
        count += 1
        if product:
          if not product.current_price:
            product.current_price = product.list_price
          if product.current_price and len(product.current_price) > 0:
            product.current_price_num = float(product.current_price.strip(' $').replace(',', ''))
          if product.inch and len(product.inch) > 0:
            try:
              product.inch_num = float(product.inch)
            except:
              pass
          if product.current_price:
            list_entities.append(product)
          if count < 5:
            self.response.write(str(product))
        product = BriefProduct(id=count)
        product.url = value
      else:
        if line.startswith('title'):
          product.title = value
          title_fields = value.split(' ') if value else []
          product.brand = title_fields[0] if value else None
          if len(title_fields) > 0:
            lower_title_fields = [x.lower() for x in title_fields]
            product.title_fields = lower_title_fields
          k = -1
          for field in title_fields:
            k += 1
            lower_f = field.lower()
            if lower_f == 'inch' and k > 0:
              product.inch = title_fields[k-1]
            elif lower_f.endswith('-inch'):
              product.inch = lower_f.split('-inch')[0]
            elif lower_f.endswith('inch'):
              product.inch = lower_f.split('inch')[0]
            elif lower_f.endswith('"'):
              product.inch = lower_f.split('"')[0]
            elif lower_f.endswith("''"):
              product.inch = lower_f.split("''")[0]
            elif (lower_f.endswith('p') and
              lower_f[0] in '0123456789'):
              product.resolution = lower_f
              try:
                product.resolution_num = float(lower_f.strip('p'))
              except:
                pass
            elif lower_f == '4k':
              product.resolution = lower_f
              product.resolution_num = 4000.0
            elif lower_f.startswith('led'):
              product.led = True
            elif lower_f.startswith('smart'):
              product.smart = True
            elif lower_f in ['tv', 'hdtv', 'uhdtv']:
              product.tv_type = lower_f
            elif lower_f.endswith('hz') and lower_f[0] in '0123456789':
              product.hz = lower_f
              try:
                product.hz_num = float(lower_f.split('hz')[0])
              except:
                pass
            elif lower_f == 'slim':
              product.slim = True
        elif line.startswith('img'):
          product.img = value
        elif line.startswith('list_price'):
          product.list_price = value
        elif line.startswith('current_price'):
          product.current_price = value
        elif line.startswith('rating'):
          product.rating = value
    if len(list_entities) > 0:
      ndb.put_multi(list_entities)
    f.close()
    self.response.write("<br>" + str(count))


app = webapp2.WSGIApplication([
  ('/upload_brief_product', UploadBriefProduct),
  ('/precompute_similar_product', PrecomputeSimilarProduct),
  ('/', Search),
], debug=True)


