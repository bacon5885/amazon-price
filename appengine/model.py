from google.appengine.ext import ndb

class BriefProduct(ndb.Model):
  title = ndb.StringProperty(indexed=True)
  rating = ndb.StringProperty(indexed=True)
  url = ndb.StringProperty(indexed=True)
  list_price = ndb.StringProperty()
  current_price = ndb.StringProperty(indexed=True)
  img = ndb.StringProperty()
  brand = ndb.StringProperty(indexed=True)
  smart = ndb.BooleanProperty(indexed=True)
  led = ndb.BooleanProperty(indexed=True)
  tv_type = ndb.StringProperty(indexed=True)
  resolution = ndb.StringProperty(indexed=True)
  resolution_num = ndb.FloatProperty(indexed=True)
  inch = ndb.StringProperty(indexed=True)
  inch_num = ndb.FloatProperty(indexed=True)
  title_fields = ndb.StringProperty(repeated=True, indexed=True)
  current_price_num = ndb.FloatProperty(indexed=True)
  hz = ndb.StringProperty(indexed=True)
  hz_num = ndb.FloatProperty(indexed=True)
  slim = ndb.BooleanProperty(indexed=True)

class SimilarProduct(ndb.Model):
  neighbors = ndb.StringProperty()
