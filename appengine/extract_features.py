#
import json

all_products = []
product = None
for line in open('brief_product_info.txt', 'r'):
  splits = line.strip(' \n').split(': ')
  value = splits[1] if len(splits) > 1 else None
  count = 0
  if line.startswith('link'):
    count += 1
    if product:
      if not product.get('current_price', None):
        product['current_price'] = product.get('list_price')
      if product.get('current_price', None) and len(product.get('current_price')) > 0:
        product['current_price_num'] = float(product.get('current_price').strip(' $').replace(',', ''))
      if product.get('inch', None) and len(product['inch']) > 0:
        try:
          product['inch_num'] = float(product['inch'])
        except:
          pass
      if product.get('current_price'):
        all_products.append(product)
        #print json.dumps(product)
    product = {}
    product['url'] = value
  else:
    if line.startswith('title'):
      product['title'] = value
      title_fields = value.split(' ') if value else []
      product['brand'] = title_fields[0] if value else None
      if len(title_fields) > 0:
        lower_title_fields = [x.lower() for x in title_fields]
        product['title_fields'] = lower_title_fields
      k = -1
      for field in title_fields:
        k += 1
        lower_f = field.lower()
        if lower_f == 'inch' and k > 0:
          product['inch'] = title_fields[k-1]
        elif lower_f.endswith('-inch'):
          product['inch'] = lower_f.split('-inch')[0]
        elif lower_f.endswith('inch'):
          product['inch'] = lower_f.split('inch')[0]
        elif lower_f.endswith('"'):
          product['inch'] = lower_f.split('"')[0]
        elif lower_f.endswith("''"):
          product['inch'] = lower_f.split("''")[0]
        elif (lower_f.endswith('p') and
          lower_f[0] in '0123456789'):
          product['resolution'] = lower_f
          try:
            product['resolution_num'] = float(lower_f.strip('p'))
          except:
            pass
        elif lower_f == '4k':
          product['resolution'] = lower_f
          product['resolution_num'] = 4000.0
        elif lower_f.startswith('led'):
          product['led'] = True
        elif lower_f.startswith('smart'):
          product['smart'] = True
        elif lower_f in ['tv', 'hdtv', 'uhdtv']:
          product['tv_type'] = lower_f
        elif lower_f.endswith('hz') and lower_f[0] in '0123456789':
          product['hz'] = lower_f
          try:
            product['hz_num'] = float(lower_f.split('hz')[0])
          except:
            pass
        elif lower_f == 'slim':
          product['slim'] = True
        elif lower_f == 'bundle':
          product['bundle'] = True
    elif line.startswith('img'):
      product['img'] = value
    elif line.startswith('list_price'):
      product['list_price'] = value
    elif line.startswith('current_price'):
      product['current_price'] = value
    elif line.startswith('rating'):
      product['rating'] = value

neighbors = []
for i in range(0, len(all_products)):
  neighbors_i = []
  for j in range(0, len(all_products)):
    if i == j:
      continue
    p1 = all_products[i]
    p2 = all_products[j]
    d = 0
    if p1.get('brand') != p2.get('brand'):
      d += 1
    if p1.get('inch_num') != p2.get('inch_num'):
      d += 2
    if p1.get('resolution') != p2.get('resolution'):
      d += 2
    if p1.get('slim') != p2.get('slim'):
      d += 1
    if p1.get('tv_type') != p2.get('tv_type'):
      d += 1
    if p1.get('bundle') != p2.get('bundle'):
      d += 1
    if d <= 0:
      neighbors_i.append(j)
  neighbors.append(neighbors_i)
  if len(neighbors_i) > 0:
    print "*********** product you are viewing:"
    print p1.get('title')
    print p1.get('current_price')
    print p1.get('url')
    print ""
    print "=========== identical products (%d):" % len(neighbors_i)
    for jj in neighbors_i:
      print all_products[jj].get('title')
      print all_products[jj].get('current_price')
      print all_products[jj].get('url')
    print ""
      
