from bs4 import BeautifulSoup
import re

for list_num in range(0,60):
  f = open('results/list_%d.html' % list_num, 'r')
  soup = BeautifulSoup(f.read())
  results_col = soup.find(id="resultsCol")
  results = results_col.find_all(id=re.compile("result_[0-9]+"))
  for result in results:
    a = result.find('a')
    print "link: " + a.get('href')
    print "img: " + a.find('img').get('src')
    h3 = result.find("h3")
    if h3:
      h3 = h3.find("span")
    print "title: " + (h3.text.encode('utf-8') if h3 else "")
    grey_price = result.find("li", "newp")
    if grey_price:
      grey_price = grey_price.find("del", "grey")
    print "list_price: " + (grey_price.text if grey_price else "")
    current_price = result.find("li", "newp")
    if current_price:
      current_price = current_price.find("span", "bld lrg red")
    if current_price:
      current_price = current_price.text
    print "current_price: " + (current_price if current_price else "")
    review = result.find("li", "rvw")
    if review:
      review = review.find('a')
    print "rating: " + (review.get('alt') if review else "")
  f.close()
